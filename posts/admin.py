from django.contrib import admin
from posts.models import Post
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.register(Post,ProjectAdmin)