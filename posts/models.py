from django.db import models
from userprofile.models import Userprofile
class Post(models.Model):
    """This model is the source of information of every post in the 
    upminsecrets app. """
    title = models.CharField(max_length=70,blank=False)
    content = models.TextField(max_length=4000,blank=False)
    points = models.IntegerField(default=0)
    posted = models.DateTimeField(auto_now_add=True)
    approved = models.NullBooleanField(default=False)
    attached_image = models.ImageField(upload_to="images/",blank=True)
    owner = models.ForeignKey(Userprofile,null=True,blank=True,related_name="posts")


    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)
        self.args = args

    def __unicode__(self):
        return self.title

    class Meta():
        ordering = ['posted',]
        verbose_name = "Post"
        verbose_name_plural = "Posts"

class Sticker(models.Model):
    image = models.ImageField(upload_to="images/stickers/",blank=False)
    title = models.CharField(max_length=10,blank=False)
    
    def __init__(self, *args, **kwargs):
        super(Sticker, self).__init__(*args, **kwargs)
        self.args = args
    
    def __unicode__(self):
        return self.title
    
    class Meta():
        ordering = ['title',]
        verbose_name = "Sticker"
        verbose_name_plural = "Stickers"

class Sticker_Post(models.Model):
    post = models.ForeignKey(Post)
    sticker = models.ForeignKey(Sticker)
    user = models.ForeignKey(Userprofile)
    posted = models.DateTimeField(auto_now=True)

    class Meta():
        ordering = ['-posted']

        
