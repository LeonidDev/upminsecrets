from userprofile.models import Userprofile
from rest_framework import serializers
from posts.models import Post,Sticker


class PostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Post
        fields = ('url','id','title','content','points',
                  'approved','attached_image','owner',
                  )
        owner = serializers.Field(source='userprofile.user')
    def restore_object(self, attrs, instance=None):
        """Create or update new instance for posts"""
        print 'test'
        if instance is not None:
            instance.title = attrs.get('title',instance.title)
            instance.content = attrs.get('content',instance.content)
            instance.points = attrs.get('points',instance.points)
            instance.approved = attrs.get('approved',instance.points)
            instance.attached_image = attrs.get('attached_image',instance.attached_image)
            return instance
        return Post(**attrs)

class StickerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sticker
        fields = ('image','title')

    def restore_object(self, attrs, instance=None):
        if instance:
            instance.title = attrs.get('title',instance.title)

class UserprofileSerializer(serializers.ModelSerializer):
    posts = serializers.HyperlinkedRelatedField(many=True, view_name="posts-detail")

    class Meta:
        model = Userprofile
        fields = ('id', 'user','posts')