from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from posts import views


urlpatterns = patterns('posts.views',
    url(r'^(?P<pk>\d+)/$', views.PostDetailView.as_view(),name="post-detail"),
)

#urlpatterns = format_suffix_patterns(urlpatterns)