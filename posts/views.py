from posts.models import Post, Sticker
from posts.serializers import PostSerializer, StickerSerializer
from userprofile.views import UserprofileView

from rest_framework import mixins, generics, status

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly


class PostsView(APIView):
    """ 
    API endpoints for listing all posts 
    or creates an individual post. 
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = PostSerializer

    def get(self, request, format=None):
        queryset = Post.objects.all()
        serializer = PostSerializer(queryset)
        return Response(serializer.data)
    

    #def post(self, request, *args, **kwargs):
        ##validation is done through the createmodelmixin
    #    return self.create(request, *args, **kwargs)
    def post(self, request, format=None):
        print request.FILES
        serializer = PostSerializer(data=request.DATA, files=request.FILES)
        if serializer.is_valid():
           serializer.save()
           return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def pre_save(self, obj):
        if self.request.user.is_authenticated():
            obj.owner = self.request.user


class PostDetailView(mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin,
                     generics.GenericAPIView):
    """
    This class-based view is responsible for updating,
    retrieving and deleting post objects.
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    def get_object(self, pk):
        try:
            return Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class StickerListView(mixins.ListModelMixin,mixins.CreateModelMixin,generics.GenericAPIView):
    """CBV listing and creating stickers"""
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = Sticker.objects.all()
    serializer_class = StickerSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
