from django.conf.urls import url, patterns, include
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, routers
from rest_framework.urlpatterns import format_suffix_patterns
from posts import views
from userprofile.views import UserprofileView
from django.contrib import admin
admin.autodiscover()
# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    model = User

class GroupViewSet(viewsets.ModelViewSet):
    model = Group


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browseable API.
urlpatterns = patterns('',
    #url(r'^', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^posts/$', views.PostsView.as_view()),
    url(r'^userprofile/$', UserprofileView.as_view()),
    url(r'^posts/', include('posts.urls',)),
)
urlpatterns = format_suffix_patterns(urlpatterns)
