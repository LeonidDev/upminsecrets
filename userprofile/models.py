from django.db import models
from django.contrib.auth.models import User
from datetime import date
BATCH_YEARS = []
[BATCH_YEARS.append((str(i),str(i))) for i in range(1995,int(date.today().year)+1) if i>1]
BATCH_YEARS = tuple(BATCH_YEARS)

class Courses(models.Model):
    BACHELOR_CHOICES = (
        ('BA','Bachelor of Arts'),
        ('BS','Bachelor of Science'),
    )
    title = models.CharField(max_length=50)
    bachelor = models.CharField(choices=BACHELOR_CHOICES,max_length=30)
    description = models.TextField(max_length=400,blank=True)

class Userprofile(models.Model):
    
    user = models.OneToOneField(User,null=True)
    moderator = models.BooleanField(default=False)
    batch = models.CharField(max_length = 4, choices=BATCH_YEARS)
    course = models.ForeignKey(Courses)

    def __init__(self, *args, **kwargs):
        super(Sticker, self).__init__(*args, **kwargs)
        self.args = args

    def __unicode__(self):
        return self.user.last_name
    
    class Meta():
        ordering = ['user__last_name',]
        verbose_name = "User Profile"
        verbose_name_plural = "User Profiles"

