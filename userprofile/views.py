from django.shortcuts import render

from rest_framework import mixins, generics
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from userprofile.models import Userprofile
from posts.serializers import UserprofileSerializer

class UserprofileView(mixins.ListModelMixin,
                      mixins.CreateModelMixin,
                      generics.GenericAPIView):

    """
    API endpoint for creating and listing all user profiles.
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = Userprofile.objects.all()
    serializer_class = UserprofileSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)